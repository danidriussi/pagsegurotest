//
//  Configuration.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import Alamofire

class Configuration: NSObject {
    
    var headers: HTTPHeaders {
        var myHeaders: [String : String] = [:]
        
        myHeaders["content-type"] = "application/json"
        
        return myHeaders
    }
    
    var baseURL: String! {
        let configuration = configurationDict()
        
        #if RELEASE
            guard let url = configuration!["baseURL"] else {
                NSLog("To bad, no baseURL for you")
                
                return ""
            }
        #else
            guard let url = configuration!["debugURL"] else {
                NSLog("To bad, no debugURL for you")
                
                guard let url = configuration!["baseURL"] else {
                    NSLog("Didn't find neither debugURL and baseURL in configuration file. ")
                    
                    return ""
                }
                
                return url as! String
            }
        #endif
        
        
        return url as! String
    }
    
    private func configurationDict() -> NSDictionary? {
        guard let path = Bundle.main.path(forResource: "PagseguroTestConfig", ofType: "plist") else {
            NSLog("Didn't find configuration file")
            
            return nil
        }
        
        return NSDictionary(contentsOfFile: path)
    }
}
