//
//  BeerListTableViewController.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

import AlamofireImage

class BeerListTableViewController: UITableViewController {
    
    var isRequestingData = false
    
    var beerList: [Beer]!
    
    var selectedBeer: Beer!
    
    @IBOutlet weak private var headerLabel: UILabel!

//MARK: View Lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInitialData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

//MARK: Initial Data setup

    func setupInitialData() {
        self.headerLabel.text = "Loading, please wait..."
        
        isRequestingData = true
        
        DataHandler.instance.getBeers(successBlock: { (beers) in
            self.beerList = beers
            self.isRequestingData = false
            
            self.headerLabel.text = "Choose your beer"
            
            let indexes: IndexSet = [0, 0]
            
            self.tableView.beginUpdates()
            self.tableView.reloadSections(indexes, with: .middle)
            self.tableView.endUpdates()
            
        }) { (errorMessage) in
            self.isRequestingData = false
            
            self.headerLabel.text = ""
            
            self.tableView.reloadData()
        }
    }

//MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isRequestingData {
            return 75.0
        }
        
        if beerList == nil || beerList.count == 0 {
            return 60.0
        }
        
        return 90.0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isRequestingData {
            return 1
        }

        if beerList == nil || beerList.count == 0 {
            return 1
        }
        
        return beerList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isRequestingData {
            return tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
        }
        
        if beerList == nil || beerList.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "noResultsCell", for: indexPath)
        }
        
        let beer = beerList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "beerCell", for: indexPath) as! BeerTableViewCell

        cell.beerName?.text = beer.name
        cell.alcoholicPercentage?.text = "\(beer.alcoholicPercentage.orNil)% ALC/VOL"

        cell.beerThumbnail?.af_setImage(withURL: URL(string: beer.imagePath!)!)
        
        return cell
    }

//MARK: Tableview delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedBeer = beerList[indexPath.row]
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "Beer detail segue", sender: self)
    }

//MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Beer detail segue" {
            let destinationVC = segue.destination as! BeerDetailTableViewController
            
            destinationVC.beer = selectedBeer
        }
    }
}

//Gotta Hate optional conversion
extension Optional {
    var orNil : String {
        if self == nil {
            return "nil"
        }
        
        if "\(Wrapped.self)" == "String" {
            return "\"\(self!)\""
        }
        
        return "\(self!)"
    }
}
