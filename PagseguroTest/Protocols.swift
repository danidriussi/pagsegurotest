//
//  Protocols.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

protocol GenericObjectSerializable: GenericObjectTranslator {
    init?(fromJSON: Dictionary<String, Any>?)
}

@objc protocol GenericObjectTranslator {
    @objc optional func extractObject()
}

extension GenericObjectTranslator {
    
    func extractObject<T: GenericObjectSerializable>(fromJSONArray json: Any) -> [T]? {
        var returnedObjects: [T] = []
        
        guard let jsonArray = json as? Array<Any> else {
            return nil
        }
        
        for dict in jsonArray {
            guard let object = T(fromJSON: dict as? Dictionary) else {
                return []
            }
            
            returnedObjects.append(object)
        }
        
        return returnedObjects
    }
}
