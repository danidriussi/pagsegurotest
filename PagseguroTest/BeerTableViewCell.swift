//
//  BeerTableViewCell.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

class BeerTableViewCell: UITableViewCell {

    @IBOutlet weak var beerThumbnail: UIImageView?
    
    @IBOutlet weak var beerName: UILabel?
    
    @IBOutlet weak var alcoholicPercentage: UILabel?
    
    @IBOutlet weak var tagline: UILabel?
    
    @IBOutlet weak var ibu: UILabel?
    
    @IBOutlet weak var descriptionLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
