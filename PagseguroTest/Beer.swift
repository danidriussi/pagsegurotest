//
//  Beer.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

class Beer: GenericObjectSerializable {

    var imagePath: String?
    
    var name: String?
    
    var alcoholicPercentage: Double?
    
    var tagline: String?
    
    var ibu: Int?
    
    var beerDescription: String? //do not use description in iOS apps =)
    
    required init?(fromJSON json: Dictionary<String, Any>?) {
        guard let json = json else {
            NSLog("Fatal error! Initializing Beer object without a json")
            
            return
        }
        
        imagePath = json["image_url"] as? String
        name = json["name"] as? String
        alcoholicPercentage = json["abv"] as? Double
        tagline = json ["tagline"] as? String
        ibu = json["ibu"] as? Int
        beerDescription = json["description"] as? String
    }
}
