
//
//  DataHandler.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

class DataHandler: NSObject, GenericObjectTranslator {
    
    static let instance = DataHandler()
    
    func getBeers(successBlock success: @escaping (_ response: [Beer]?) -> Void, failureBlock failure: @escaping (_ message: String) -> Void) {
        
        HttpClient.instance.doGET(withPath: "beers", successBlock: { (response) in
            guard response is Array<Any> else {
                return
            }
            
            let beers: [Beer] = self.extractObject(fromJSONArray: response)!

            success(beers)
            
        }) { (errorMessage) in
            failure(errorMessage)
        }
        
    }
}
