//
//  BeerDetailTableViewController.swift
//  PagseguroTest
//
//  Created by Daniel Novio on 01/06/17.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

import AlamofireImage

class BeerDetailTableViewController: UITableViewController {
    
    var beer: Beer!

//MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.title = beer.name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

//MARK: Tableview datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = BeerTableViewCell()
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Beer info cell", for: indexPath) as! BeerTableViewCell
            
        } else if indexPath.row == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Beer description cell", for: indexPath) as! BeerTableViewCell
        }
        
        cell.beerThumbnail?.af_setImage(withURL: URL(string: beer.imagePath!)!)
        
        cell.beerName?.text = beer.name
        cell.tagline?.text = beer.tagline
        cell.alcoholicPercentage?.text = "\(beer.alcoholicPercentage.orNil)% ALC/VOL"
        cell.ibu?.text = "IBU: \(beer.ibu.orNil)"
        
        cell.descriptionLabel?.text = beer.beerDescription
        
        return cell
    }
}
